// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      title: 'JS Developer',
      meta: [
        { charset: 'utf-8' },
        {
          name: 'viewport',
          content:
            'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'
        },
        {
          hid: 'description',
          name: 'description',
          content: 'Website jsdeveloper.uz created for JavaScript lovers!'
        },
        {
          name: 'keywords',
          content: 'JSDEVELOPER.uz, jsdeveloper.uz, Javascript UZ, JavaScript content in Uzbek'
        },
        {
          'http-equiv': 'Content-Type',
          content: 'text/html',
          charset: 'UTF-8'
        }
      ]
    }
  },
  devtools: { enabled: false },
  modules: [
    '@nuxtjs/tailwindcss',
    'shadcn-nuxt',
    '@nuxtjs/color-mode',
    '@sidebase/nuxt-auth'
  ],
  auth: {
    baseURL: process.env.API_BASE_URL,
    provider: {
      type: 'local',
      pages: {
        login: '/auth'
      },
      endpoints: {
        signIn: {
          path: '/auth/check-credentials',
          method: 'post'
        },
        signOut: {
          path: '/auth/sign-out',
          method: 'post'
        },
        getSession: {
          path: '/users/me',
          method: 'get'
        }
      },
      token: {
        signInResponseTokenPointer: '/access_token'
      }
    }
  },
  shadcn: {
    /**
     * Prefix for all the imported component
     */
    prefix: '',
    /**
     * Directory that the component lives in.
     * @default "./components/ui"
     */
    componentDir: './components/ui'
  },
  colorMode: {
    classSuffix: ''
  },
  tailwindcss: {
    configPath: 'tailwind.config.ts'
  },
  css: [
    './assets/css/main.css'
  ]
})
